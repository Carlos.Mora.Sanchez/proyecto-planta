let sitio = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      properties: {
        popupContent: "THIS IS MY BEAN CROP",
        descripcion: {
          Nombre_Agricultor: "Carlos Eduardo Mora Sanchez",
          Tipo_Planta: "Frijol",
          Nombre_Cientifico: "Phaseolus Vulgaris Cranberry Group",
          Fecha_Plantacion: "2021/01/07",
        },
      },
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [-74.02097046375275, 4.7439952838935175],
            [-74.02079343795775, 4.7439952838935175],
            [-74.02079343795775, 4.744663538629481],
            [-74.02097046375275, 4.744663538629481],
            [-74.02097046375275, 4.7439952838935175],
          ],
        ],
      },
    },
    {
    },
  ],
};
