let miMapa = L.map("mapid");

miMapa.setView([4.744066156230049, -74.0208571530804], 18);

let miProveedor = L.tileLayer(
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }
);
miProveedor.addTo(miMapa);

let miGeoJSON = L.geoJSON(sitio);

miGeoJSON.addTo(miMapa);

let miTitulo = document.getElementById("titulo");
miTitulo.textContent = sitio.features[0].properties.popupContent;
var customIcon = new L.Icon({
  iconUrl: 'Img/sapling.svg',
  iconSize: [50, 50],
  iconAnchor: [25, 50]
});
let miMarcador = L.marker([4.744040725236051, -74.02083098888397], {icon: customIcon});
miMarcador.addTo(miMapa);
var circle = L.circle([4.744177049245737, -74.02082562446594], {
    color: 'dark Green',
    fillColor: 'green',
    fillOpacity: 0.6,
    radius: 20
});
circle.addTo(miMapa);

miMarcador
  .bindPopup(
    "Nombre Proyecto: Virtual Green Farm.</br><br>Nombre agricultor: Carlos Eduardo Mora Sanchez.<br>Tipo de planta:Frijol<br>Nombre cientifico: Phaseolus Vulgaris Cranberry Group.<br>Fecha de plantacion: 2021/01/07."
  )
  .openPopup();
miMarcador.addTo(miMapa);
